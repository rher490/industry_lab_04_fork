package ictgradschool.industry.lab04.ex04;

/**
 * A game of Rock, Paper Scissors
 */
import ictgradschool.Keyboard;
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int PAPER = 2;
    public static final int SCISSORS = 3;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.print("Hi! What is your name? ");
        String name = ictgradschool.Keyboard.readInput();
        boolean keepPlaying = true;
        while (keepPlaying) {
            System.out.println("1. Rock\n2. Scissor\n3. Paper\n4. Quit");
            System.out.print("Enter choice: ");
            int playerChoice = Integer.parseInt(Keyboard.readInput());
            System.out.println();
            if (playerChoice == 4) {
                System.out.println("Goodbye " + name + ". Thanks for playing :)");
                keepPlaying = false;
            } else {
                displayPlayerChoice(name, playerChoice);
                int computer = (int) (Math.random() * 3 + 1);
                displayPlayerChoice("Computer", computer);
                if (playerChoice == computer) {
                    System.out.println(getResultString(playerChoice, computer));
                } else if (userWins(playerChoice, computer)) {
                    System.out.println(name + " wins because " + getResultString(playerChoice, computer));
                } else {
                    System.out.println("Computer wins because " + getResultString(computer, playerChoice));
                }
//                System.out.println(getResultString(playerChoice,computer));
            }
            System.out.println();
        }
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        if (choice == ROCK) {
            System.out.println(name + " chose rock.");
        } else if (choice == SCISSORS) {
            System.out.println(name + " chose scissors.");
        } else {
            System.out.println(name + " chose paper.");
        }
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if ((playerChoice + 1) == computerChoice) {
            return false;
        } else return !(playerChoice == SCISSORS && computerChoice == ROCK);
    }

    public String getResultString(int option, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = "No one wins." /*you chose the same as the computer"*/;

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (option == computerChoice) {
            return TIE;
        } else if (option == PAPER) {
            return PAPER_WINS;
        } else if (option == ROCK) {
            return ROCK_WINS;
        } else if (option == SCISSORS) {
            return SCISSORS_WINS;
        } else {
            return "You missed something.";
        }
    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
