package ictgradschool.industry.lab04.ex05;

/**
 * Created by rher490 on 14/11/2017.
 */
public class Pattern {
    private int number;
    private char c;

    public Pattern(int number, char c) {
        this.number = number;
        this.c = c;

    }

    public int getNumberOfCharacters() {
        return number;
    }

    public void setNumberOfCharacters(int number) {
        this.number = number;
    }

    public String toString() {
        String s = "";
        for (int i = 0; i < number; i++) {
            s += c;
        }
        return s;
    }
}
