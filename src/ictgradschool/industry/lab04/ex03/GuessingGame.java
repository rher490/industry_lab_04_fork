package ictgradschool.industry.lab04.ex03;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int n = (int) (Math.random() * 101 + 1);
        int guess = -1;
        while (guess != n) {
            guess = promptUser(n);


        }
        System.out.println("Perfect!");
        System.out.println("Goodbye");

    }

    private int promptUser(int x) {
        System.out.print("Enter your guess (1 - 100): ");
        String s = ictgradschool.Keyboard.readInput();
        int guess = Integer.parseInt(s);
        if (guess > x) {
            System.out.println("Too high, try again");
        } else if (guess < x) {
            System.out.println("Too low, try again");
        }
        return guess;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
